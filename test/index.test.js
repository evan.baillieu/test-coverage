import {
  fibo,
  my_alpha_number_t,
  my_array_alpha_t,
  my_display_alpha_reverse_t,
  my_display_alpha_t,
  my_display_unicode_t,
  my_is_posi_neg_t,
  my_length_array_t,
  my_size_alpha_t,
  sum
} from '../src';

describe('Index', () => {
  describe('my_alpha_number_t', () => {
    it('Should my_alpha_number_t return good string ', () => {
      expect(my_alpha_number_t(3)).toBe('3');
    });
  });

  describe('sum', () => {
    it('Should be check sum return params1 + params2', () => {
      expect(sum(1, 2)).toBe(3);
    });

    it('Should check sum return zero for bad params', () => {
      expect(sum(1, 'test')).toBe(0);
      expect(sum('', 1)).toBe(0);
    });
  });

  describe('my_size_alpha_t', () => {
    it('Should check my_size_alpha_t return number of the char', () => {
      expect(my_size_alpha_t('test')).toBe(4);
    });

    it('Should be check my_size_alpha_t return zero for not a string params', () => {
      expect(my_size_alpha_t(1)).toBe(0);
    });
  });

  describe('my_display_alpha_t', () => {
    it('Should be check my_display_alpha_t return aplabet', () => {
      expect(my_display_alpha_t()).toBe('abcdefghijklmnopqrstuvwxyz');
    });
  });

  describe('my_array_alpha_t', () => {
    it('Should be check my_array_alpha_t return array', () => {
      expect(my_array_alpha_t('test')).toStrictEqual(['t', 'e', 's', 't']);
    });
  });

  describe('my_is_posi_neg_t', () => {
    it('Should be check my_is_posi_neg_t check number is postif', () => {
      expect(my_is_posi_neg_t(1)).toBe('POSITIF');
    });

    it('Should be check my_is_posi_neg_t check number is negatif', () => {
      expect(my_is_posi_neg_t(-1)).toBe('NEGATIVE');
    });
  });

  describe('fibo', () => {
    it('Should be check fibo return value of fibonatie for params 5', () => {
      expect(fibo(5)).toBe(5);
    });

    it('Should be check fibo return value of fibonatie for params 1, 2', () => {
      expect(fibo(1)).toBe(1);
      expect(fibo(2)).toBe(1);
    });

    it('Should be check fibo return value of fibonatie for params negatif', () => {
      expect(fibo(-1)).toBe(0);
    });
  });

  describe('my_display_alpha_reverse_t', () => {
    it('Should be check my_display_alpha_reverse_t return revert string', () => {
      expect(my_display_alpha_reverse_t()).toBe('zyxwvutsrqponmlkjihgfedcba');
    });
  });

  describe('my_length_array_t', () => {
    it('Should be check my_length_array_t', () => {
      const params = ['1', '2', '3'];
      expect(my_length_array_t(params)).toBe(params.length);
    });
  });

  describe('my_display_unicode_t', () => {
    it('Should be check my_display_unicode_t', () => {
      expect(my_display_unicode_t([66, 98, 49, 32])).toBe('Bb1 ');
    });
  });
});
